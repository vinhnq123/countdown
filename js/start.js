$(document).ready(function() {
    if ($(".slider-for").length > 0) {
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            autoplay: true,
            nav: false,
            focusOnSelect: true
        });
    }

    $('.owl-carousel-1').owlCarousel({
        loop: true,
        items: 1,
        nav: false,
        dots: true,
        autoplay: true,
        smartSpeed: 900
    });
    $('.who-carousel').owlCarousel({
        loop: true,
        items: 1,
        nav: true,
        dots: true,
        smartSpeed: 1000,
        autoplay: true
    });

    $('.sub-carousel').owlCarousel({
        loop: true,
        items: 1,
        nav: false,
        dots: true,
        smartSpeed: 1000,
        autoplay: true
    });


    $('.owl-news').owlCarousel({
        loop: true,
        margin: 30,
        items: 4,
        nav: true,
        smartSpeed: 900,
        responsive: {
            0: {
                items: 1.25,
                nav: true
            },
            600: {
                items: 2,
                nav: true,
            },
            1025: {
                items: 3,
                nav: true,
                loop: true,
            },
            1441: {
                items: 4,
                nav: true,
                loop: true,
            },
            1600: {
                items: 4,
                nav: true,
                loop: true
            }
        }
    });


    $('.owl-whycleansui').owlCarousel({

        items: 6,
        nav: false,
        margin: 28,
        dots: true,
        smartSpeed: 1000,
        autoplay: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 3,
                nav: false
            },
            1025: {
                items: 4,
                nav: false

            },
            1441: {
                items: 6,
                nav: false

            },
            1600: {
                items: 6,
                nav: false

            }
        }
    });

    //start owl nav thumb

    var bigimage = $("#big");
    var thumbs = $("#thumbs");

    var syncedSecondary = true;

    bigimage
        .owlCarousel({
            items: 1,
            slideSpeed: 10,
            nav: false,
            autoplay: true,
            dots: false,
            loop: true,
            responsiveRefreshRate: 2,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            navText: [
                '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
                '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
            ]
        })
        .on("changed.owl.carousel", syncPosition);

    thumbs
        .on("initialized.owl.carousel", function() {
            thumbs
                .find(".owl-item")
                .eq(0)
                .addClass("current");
        })
        .owlCarousel({
            items: 6,
            dots: false,
            autoplay: true,
            nav: false,
            navText: [
                '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
                '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
            ],
            smartSpeed: 200,
            slideSpeed: 500,
            slideBy: 6,
            responsiveRefreshRate: 100
        })
        .on("changed.owl.carousel", syncPosition2);

    function syncPosition(el) {
        //if loop is set to false, then you have to uncomment the next line
        //var current = el.item.index;

        //to disable loop, comment this block
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

        if (current < 0) {
            current = count;
        }
        if (current > count) {
            current = 0;
        }
        //to this
        thumbs
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = thumbs.find(".owl-item.active").length - 1;
        var start = thumbs
            .find(".owl-item.active")
            .first()
            .index();
        var end = thumbs
            .find(".owl-item.active")
            .last()
            .index();

        if (current > end) {
            thumbs.data("owl.carousel").to(current, 100, true);
        }
        if (current < start) {
            thumbs.data("owl.carousel").to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            bigimage.data("owl.carousel").to(number, 100, true);
        }
    }

    thumbs.on("click", ".owl-item", function(e) {
        e.preventDefault();
        var number = $(this).index();
        bigimage.data("owl.carousel").to(number, 300, true);
    });

    //end thummd slider

    var controls = document.querySelectorAll('.openitem');

    // you can use forEach here too
    [].forEach.call(controls, el => {
        el.addEventListener('click', btnClick, false)
    })

    function btnClick() {
        // use Array function for lexical this
        [].forEach.call(controls, el => {
            // except for the element clicked, remove active class
            if (el !== this) el.classList.remove('active');
        });

        // toggle active on the clicked button
        this.classList.toggle('active');
    }

    // animation
    wow = new WOW({
        animateClass: 'animated',
        offset: 100,
        callback: function(box) {
            console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
    });
    wow.init();
    // scroll add active timeline
    if ($(".history").length > 0) {
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            var objectSelect = $(".history");
            var objectPosition = objectSelect.offset().top - 100;
            if (scroll > objectPosition) {
                $(".timeline").addClass("active");
            }
        });
    }


    if ($(".btn-menu").length > 0) {
        $(".btn-menu").click(function() {
            if ($("body").hasClass("menu")) {
                $("body").removeClass("menu");
            } else {
                $("body").addClass("menu");
                return false;
            }
        });
    }
    if ($(".header-mobile").length > 0) {
        $('.header-mobile .dropdown').on('shown.bs.dropdown', function() {
            $(".header-mobile ul > li.active").addClass("deactive");
        });
        $('.header-mobile .dropdown').on('hidden.bs.dropdown', function() {
            $(".header-mobile ul > li.active").removeClass("deactive");
        })
    }
});

// add class active p4
$(window).scroll(function(event) {
    var scroll = $(window).scrollTop();
    $('#hollowFiberContainer').addClass('active');
});

$('.box-info').click(function(event) {
    event.stopPropagation();
});